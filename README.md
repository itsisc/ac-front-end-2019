
# Welcome to AC Front-end!
În primul și primul rând am vrea să începem prin a vă mulțumi pentru faptul că sunteți astăzi aici și să vă felicităm pentru toată munca pe care ați depus-o până acum în cadrul întregului proces de recrutare. **Well done!**

![enter image description here](https://media.giphy.com/media/MOWPkhRAUbR7i/giphy.gif)

Acum, pentru ceea ce urmează, vă îndemnăm să vă încărcați cu și mai multă energie și bună dispoziție și vă promitem că fiecare moment și fiecare minut dedicat **SiSC**-ului va merita din plin.
**Și pentru că suntem IT și ne place eficiența, să ne focusăm asupra acestui aspect acum.**
**![](https://lh5.googleusercontent.com/qweNk3Go8pfwImBqBgmuCJstnD4BnJfOEtjzS_cLBg6oVs3BlggYMBMY8pGDuFceJtIQNez8Hb-gsaO3-b1Hf_ey4sMHwv0Lr_CyXRD4FLyk7bxd2Umu6iROZChkszbnGkEptE2I)**

Pentru început, după cum probabil ați realizat, **Git** este o unealtă de bază pentru noi. Aici ajung toate proiectele noastre și de aici pleacă mai departe toate creațiile către publicul larg.
Pentru că ne oferă posibilitatea să menținem și lucrăm întotdeauna pe ultima versiune a fișierelor, acesta aduce un avantaj enorm unei **echipe** care colaborează constant, deci o să vă rugăm și pe voi să îi acordați la fel de multă importanță și să fiți atenți în momentul în care vom explica funcționalitățile de bază.

![enter image description here](https://media.giphy.com/media/3ohuAxV0DfcLTxVh6w/giphy.gif)
  
Puțin mai jos vom lista și **necesarele + materialele** pe care vi le-am pus la dispoziție acum câteva zile, în cazul în care va fi nevoie să le accesați ceva mai rapid:
*Tutoriale:*
 - [**HTML&CSS**](https://www.khanacademy.org/computing/computer-programming/html-css)
 - [**JavaScript**](https://www.learn-js.org/)
 
 *Programe de instalat:*
 
 - **[Visual Studio Code](https://code.visualstudio.com/)**
 - **[Git](https://git-scm.com/downloads)**

![enter image description here](https://media.giphy.com/media/2juvZoQ3oLa4U/giphy.gif)
# Acum, să trecem la treabă!
## **Ce veți avea de făcut mai exact?**
Prima dată va trebui să facem rost de materialele cu care vom lucra. Pentru a face asta deschidem:
-   **Linia de comandă** - dacă avem Ubuntu
-   **[Git bash](https://git-scm.com/download/win)** - dacă avem Windows
Vom crea un folder, iar aici vom face practic **o clonă** la tot ceea ce v-am pregătit, folosind comanda:
`git clone REPO_HTTPS_ADDRESS`
În cazul nostru:
`git clone https://gitlab.com/itsisc/ac-front-end-2019.git`

Acum, dacă navigăm în folderul de mai sus...

![enter image description here](https://user-content.gitlab-static.net/b108b19430b1df98d9389bbd3133b0941e1af5e8/68747470733a2f2f692e67697068792e636f6d2f6d656469612f724b7a784530513545514b46712f67697068792e77656270)

...vom găsi un sub-folder nou, cu toate fișierele din acest **repository**.
 Acolo se regăsește conținutul unui **site** , atât partea de **HTML**, cât și cea de **CSS**, câteva **imagini**, dar și o schemă la care vom reveni puțin mai târziu. Acest site vine cu anumite **cerințe** la pachet, de care va trebui să ne ocupăm astăzi.
## **Ce avem de făcut?**

În primul rând să vedem cu ce avem de-a face. Pentru asta vom deschide **index.html** .

![enter image description here](https://media.giphy.com/media/6tjE0uFwyqrgA/giphy.gif) 

Nu e chiar ceea ce te așteptai să vezi, nu? 
Se pare că din motive necunoscute site-ul nu arată întocmai cum ne dorim, de aceea voi va trebui să-l aranjați. Pentru a avea un punct de reper legat de cum ar trebui să arate site-ul vom deschide **mockup.png**, aflat în fișierul menționat mai sus.

**De unde începem?**

Desigur, pentru a ne ocupa de probleme, va fi nevoie să ne murdărim puțin pe mâini cu ceva cod.
 Așadar, vom deschide acum **Visual Studio Code**. După ce am intrat în program vom merge în colțul din stânga, la **File** și vom selecta de acolo **Open Folder**. În continuare căutăm folder-ul care ne interesează pentru a putea să ne apucăm de treabă.
 
Acum că am văzut cum arată partea din spatele unei pagini web, a sosit momentul să ne ocupăm de defectele acesteia. 

![enter image description here](https://media.giphy.com/media/l49JHz7kJvl6MCj3G/giphy.gif)

**Felicitări!** 
 Acum că avem un site prezentabil, hai să îi anunțăm și pe ceilalți că problema a fost rezolvată și să urcăm codul sursă undeva... Evident, pe Git.
-   Mergem pe profilul nostru de **Gitlab** și dăm click pe butonul mare și verde cu **New Project**.
  
 -   Numele proiectului va fi **ac front 2019**
 -   Nivelul de vizibilitate va fi **Public**
 -   Și vom bifa căsuța pentru **README.md**, pentru a-l clona mai ușor pe local
 -    În descriere o să adăugați **Numele și Prenumele** vostru
  
> Important de menționat că ori de câte ori veți face modificări și veți vrea să le urcați pe Git va trebui să urmați aceiași pași. Deci atenție, pentru că nu e nimic dificil și merită din plin.
-   Pentru a verifica dacă există diferențe între repository și folderul local rulăm:
  
 ```
 git status 
 ```
  
-   Pentru a adăuga un fișier modificat din listă avem:
  
 ```
 git add FILE_NAME
 ```
  
-   În caz că vrem să le adăugăm pe **toate**:
  
 ```
 git add .
 ```
  
-   Acum putem rula din nou **git status**, să vedem ce fișiere au fost adăugate cu succes.
  
-   Iar pentru ca toate modificările să aibă sens și toți ceilalți colaboratori să înțeleagă care a fost scopul update-ului, trebuie să oferim și un mesaj specific:
  
 ```
 git commit -m "YOUR_MESSAGE_HERE"
  
 ex. -->  git commit -m "Fixed Styling Problems"
 ```
  
-   În final vom folosi o comandă ce va trimite toată informația către proiectul pe care tocmai ce l-ați creat. Sună cam așa:
  
 ```
 git push
 ```
![enter image description here](https://user-content.gitlab-static.net/d02c993ee3aece3924e9c637fc7d60ce27d502d1/68747470733a2f2f692e67697068792e636f6d2f6d656469612f37787036753530543044756c612f67697068792e77656270)
